#! /usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import random
import json
import datetime
from flask import render_template, jsonify, request

from . import parking
from . import conditions
from refuge import app, RESTAURANTS, AREAS, jc_decaux_api_key, process_itinerary

def weighted_choice(choices):
    """
    A weighted version of random.choice().
    """
    total = sum(w for c, w in choices)
    r = random.uniform(0, total)
    upto = 0
    for c, w in choices:
        if upto + w > r:
            return c
        upto += w
    assert False, "Shouldn't get here"

def select_restaurant(method='GET', latitude=None, longitude=None):
    """
    Returns a restaurant in a pseudo-random fashion.
    """
    # Filters available restaurants (from the file restaurants.json)
    # based on conditions defined in conditions.py.
    result = None
    restaurants_not_far = RESTAURANTS

    if None != latitude and None != longitude:

        for area in AREAS.keys():
            if latitude <= AREAS[area][0] and \
                        latitude >= AREAS[area][2] and \
                        longitude >= AREAS[area][1] and \
                        longitude <= AREAS[area][3]:

                restaurants_not_far = []
                for restaurant in RESTAURANTS:
                    if restaurant["coordinates"]["lat"] <= AREAS[area][0] and \
                        restaurant["coordinates"]["lat"] >= AREAS[area][2] and \
                        restaurant["coordinates"]["long"] >= AREAS[area][1] and \
                        restaurant["coordinates"]["long"] <= AREAS[area][3]:

                        restaurants_not_far.append(restaurant)
                break
        else:
            # user is not in a known area
            print("User is not in a known area. So we finally keep all restaurants.")
            restaurants_not_far = RESTAURANTS

        if len(restaurants_not_far) == 0:
            print("All restaurants are to far. So we finally keep all restaurants.")
            restaurants_not_far = RESTAURANTS


    currently_available_restaurants = [ dict((k , v) for k,v in restaurant.items() if k != "condition") \
                                for restaurant in restaurants_not_far \
                                if \
                                all([getattr(conditions, condition)() for condition in restaurant.get("condition", "default").split(";") if hasattr(conditions, condition)])==True
                              ]

    if method == 'POST':
        # The user has sent his preferences in a JSON file.
        try:
            weights = request.json
        except:
            result = jsonify({"error":"invalid JSON data"})
        dic = {elem['name']:elem['weight'] for elem in weights}
        weighted_restaurants = []
        for restaurant in currently_available_restaurants:
            weighted_restaurants.append( (restaurant, dic.get(restaurant["name"], 0)) )
        try:
            result = weighted_choice(weighted_restaurants)
        except AssertionError:
            result = {"error":"no result"}

    if method == 'GET':
        # In this case, simply returns a random restaurant.
        if currently_available_restaurants == []:
            result = jsonify({"error":"no result"})
        result = random.choice(currently_available_restaurants)

    return result


@app.route('/json', methods=['GET', 'POST'])
def restaurant_json():
    """This view returns a JSON with the selected restaurant.
    """
    try:
        latitude = float(request.args.get('latitude', 49.6045473))
        longitude = float(request.args.get('longitude', 6.1354695))
    except:
        latitude, longitude = 49.6045473, 6.1354695

    selected_restaurant = select_restaurant(request.method, latitude, longitude)

    coords = ((longitude, latitude),
                (selected_restaurant['coordinates']['long'],
                selected_restaurant['coordinates']['lat'])
    )

    transport, distance, itinerary = process_itinerary(coords)

    selected_restaurant["transport"] = transport
    selected_restaurant["distance"] = distance
    selected_restaurant["shortest_path"] = [{"lat":point[1], "long":point[0]}
                                                for point in itinerary]

    return jsonify(selected_restaurant)

@app.route('/', methods=['GET', 'POST'])
def template_html():
    """
    Returns a restaurant with the itinerary displayed in an OpenStreetMap.
    """
    return render_template('restaurant.html')

@app.route('/list/json', methods=['GET'])
def list_restaurants_json():
    """
    Returns a list of all available restaurants in JSON.
    """
    return jsonify(restaurants=RESTAURANTS)

@app.route('/list', methods=['GET'])
def list_restaurants():
    """
    Returns a list of all available restaurants in an OpenStreetMap.
    """
    stations = []
    if jc_decaux_api_key != None:
        try:
            result = requests.get("https://api.jcdecaux.com/vls/v1/stations?contract=Luxembourg&apiKey="+jc_decaux_api_key)
            if result.status_code == 200:
                stations = json.loads(result.content.decode('utf-8'))
        except:
            pass
    parkings = parking.get_parkings()
    return render_template('restaurants.html', stations=stations, parkings=parkings)

@app.errorhandler(404)
def not_found(error=None):
    """
    Handles 404 errors.
    """
    message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp
