/**
* Function: addMarker
* Add a new marker to the layer given the following lonlat,
*     popupClass, and popup contents HTML. Also allow specifying
*     whether or not to give the popup a close boX.
*
* Parameters:
* ll - {<OpenLayers.LonLat>} Where to place the marker
* popupClass - {<OpenLayers.Class>} Which class of popup to bring up
*     when the marker is clicked.
* popupContentHTML - {String} What to put in the popup
* closeBox - {Boolean} Should popup have a close box?
* overflow - {Boolean} Let the popup overflow scrollbars?
*/
function addMarker(ll, popupClass, popupContentHTML, closeBox, overflow, icon, layer) {

    var feature = new OpenLayers.Feature(layer, ll);
    feature.closeBox = closeBox;
    feature.popupClass = popupClass;
    feature.data.popupContentHTML = popupContentHTML;
    feature.data.overflow = (overflow) ? "auto" : "hidden";
    feature.data.icon = new OpenLayers.Icon(icon);

    var marker = feature.createMarker();

    var markerClick = function (evt) {
        if (this.popup == null) {
            this.popup = this.createPopup(this.closeBox);
            map.addPopup(this.popup);
            this.popup.show();
        } else {
            this.popup.toggle();
        }
        currentPopup = this.popup;
        OpenLayers.Event.stop(evt);
    };
    marker.events.register("mousedown", feature, markerClick);

    layer.addMarker(marker);
}


var X = document.getElementById("geolocation");
var DEFAULT_LATITUDE = 49.6045473;
var DEFAULT_LONGITUDE = 6.1354695;


function getLocation()
{
    X = document.getElementById("geolocation");
    if (navigator.geolocation)
    {
        var optn = {
            enableHighAccuracy : true,
            timeout : 10000,
            maximumAge : 0
        };
        navigator.geolocation.getCurrentPosition(init, showError, optn);
    }
    else
    {
        X.innerHTML = '<span class="glyphicon glyphicon-warning-sign"></span> Geolocation is not supported by this browser. Automatically set to (49.6045473, 6.1354695).';
        get_restaurant(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
    }
}

function showError(error)
{
    var error_message = '<span class="glyphicon glyphicon-warning-sign"></span> ';
    switch(error.code)
    {
        case error.PERMISSION_DENIED:
            error_message += 'User denied the request for geolocation.';
            break;
        case error.POSITION_UNAVAILABLE:
            error_message += 'Location information is unavailable.';
            break;
        case error.TIMEOUT:
            error_message += 'The request to get user location timed out.';
            break;
        case error.UNKNOWN_ERROR:
            error_message += 'An unknown error occurred.';
            break;
    }
    X.innerHTML = error_message + ' Location automatically set to (49.6045473, 6.1354695).'
    get_restaurant(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
    
}
