#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
from flask import Flask

import openrouteservice
from openrouteservice import convert
from openrouteservice.directions import directions


app = Flask(__name__)
app.config.from_object(__name__)

# Loads the restaurants
RESTAURANTS = {}
with open('./refuge/var/restaurants.json', 'r') as f:
    RESTAURANTS = json.load(f)

AREAS = {
            "Kirchberg": [49.6383, 6.1349, 49.6228, 6.1785],
            "Gare": [49.6088, 6.1206, 49.5953, 6.1472],
            "Esch-sur-Alzette": [49.5532, 5.8868, 49.4896, 5.9991],
            "Sierck-Les-Bains": [49.4770, 6.3202, 49.4154, 6.4342],
            "Sarreguemines": [49.3323, 6.5286, 48.9274, 7.5157]
        }

jc_decaux_api_key = os.environ.get('JCDecauxAPIKEY', None)
openrouteservice_api_key = os.environ.get('openrouteserviceAPIKEY', None)


def process_itinerary(coords):
    """Calculate the itinerary between two points store in coords:
    coords = ((lat1, long1), (lat2, long2))
    """
    transport = 'foot'
    if not openrouteservice_api_key:
        # not API key is defined
        return '', 0, []
    try:
        client_ors = openrouteservice.Client(key=openrouteservice_api_key)
    except:
        return '', 0, []
    routes = directions(client_ors, coords, 'foot-walking')
    distance = routes['routes'][0]['summary']['distance']
    if distance >= 4000:
        # if distance is more than 4000 meters, better to not go by foot
        transport = 'car'
        routes = directions(client_ors, coords)
        distance = routes['routes'][0]['summary']['distance']
    geometry = routes['routes'][0]['geometry']
    path = convert.decode_polyline(geometry)['coordinates']
    return transport, distance, path


import refuge.views
