#! /usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import requests
import json

def default():
    """
    Default function, called if no 'condition' function is defined for the restaurant (in restaurants.json).
    """
    return True

def quick():
    """
    Quick only on fridays.
    """
    return datetime.datetime.today().weekday() == 4

def not_on_fridays():
    """
    Test if today is not friday.
    """
    return not quick()

def before_11h45():
    """
    Return True if it is not later than 11:45 a.m..
    """
    return datetime.datetime.strptime("11:45", "%H:%M").time() > datetime.datetime.now().time()

def rain():
    """
    Return True if it is not raining.
    This test can be used for the 'Chambre de Commerce' because it is far from us.
    """
    try:
        result = requests.get("http://api.openweathermap.org/data/2.5/weather?q=Luxembourg,Lux")
    except Exception as e:
        print(str(e))
        return True

    if result.status_code != 200:
        print(result.status_code)
        return True

    json_result = json.loads(result.text)
    try:
        return json_result['weather'][0]['main'] != 'Rain'
    except Exception as e:
        print(str(e))
        return False
