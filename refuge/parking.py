#! /usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from xml.dom.minidom import parseString

class Parking(object):
    """
    Defines a parking.
    """
    def __init__(self, title="", link="", total_places=0, available_places=0, \
                    latitude=0.0, longitude=0.0, picture=""):
        self.title = title
        self.link = link
        self.total_places = total_places
        self.available_places = available_places
        self.latitude = latitude
        self.longitude = longitude
        self.picture = picture

def get_parkings():
    """
    Retrieves information about the parkings of the city of Luxembourg.
    """
    result = []
    try:
        request = requests.get("https://feed.vdl.lu/circulation/parking/feed.rss")
        if request.status_code == 200:
            parkings = parseString(request.content)
        else:
            return result
    except:
        return result
    for parking in parkings.getElementsByTagName('item'):
        title = parking.getElementsByTagName('title')[0].childNodes[0].nodeValue
        link = parking.getElementsByTagName('guid')[0].childNodes[0].nodeValue
        try:
            total_places = parking.getElementsByTagName('vdlxml:total')[0].childNodes[0].nodeValue
        except:
            total_places = "no data"
        try:
            available_places = parking.getElementsByTagName('vdlxml:actuel')[0].childNodes[0].nodeValue
        except:
            available_places = "no data"

        latitude = parking.getElementsByTagName('vdlxml:localisation')[0]. \
                    getElementsByTagName('vdlxml:localisationLatitude')[0].childNodes[0].nodeValue
        longitude = parking.getElementsByTagName('vdlxml:localisation')[0]. \
                    getElementsByTagName('vdlxml:localisationLongitude')[0].childNodes[0].nodeValue
        try:
            picture = parking.getElementsByTagName('vdlxml:divers')[0]. \
                        getElementsByTagName('vdlxml:pictureUrl')[0].childNodes[0].nodeValue
        except:
            picture = ''

        result.append(Parking(title, link, total_places, available_places, \
                                latitude, longitude, picture))

    return result
