=========
Changelog
=========

v0.9 - 2014-02-24
=================

Commit: 3a2e57214a9a51e1bfb4081fab83fc924a634d07

New:

* Retrieves information about the parkings of the city of Luxembourg.


v0.8 - 2014-02-23
=================

Commit: 40b6753dfcd7b7a9d1c4510f2293c141fe7d4bda

New:

* Bootstrap integration;
* JCDecaux API integration (for the bikes of the city of Luxembourg);
* List of all restaurants displayed on the map;
* The map manages OpenStreetMap and Google Maps layers.

Improvements:

* improvements of the localization JavaScript code;


v0.7 - 2014-02-20
=================

Commit: 95cd1e43b15f5864821db2176921c824c15dc464

New:

* Geolocalization with HTML5 API. The HTML view is now using the embedded JSON service (jQuery AJAX get().)


v0.6 - 2014-02-19
=================

Commit: 9477f951106f30bef76bbb8df482a3e6c49d8656

New:

* Client side voice synthesis with espeak (JavaScript implementation).


v0.5 - 2014-02-17
=================

Commit: b2ee3a13a2d5cafacbc160a8720f618938150c92

New:

* Generation of the itinerary to the selected restaurant with Dijkstra (local algorithm).


v0.4 - 2014-02-11
=================

Commit: ee823e25b80df747199624a77558ea9ec271b0fa

New:

* The application can now be deployed both on Heroku and Google App Engine.


v0.3 - 2014-01-24
=================

Commit: 90edce9ae6922688aed2d9603bc54d603b482492

New:

* The possibility to send its preferences is now offered to the user (via a JSON file).

Improvements:

* It is now possible to specify a list of conditions for each restaurants (in restaurants.json).


v0.2 - 2014-01-20
=================

Commit: dd32515328c8b328cf27e232df9904a83be0c355

New:

* It is now possible to specify a condition (distance, weather conditions, etc.) for each restaurants.

Improvements:

* The list of restaurants is stored in a json file (restaurants.json).


v0.1 - 2014-01-15
=================

Commit: 2b253652468a1e5b02e1864edec595c1d8fb212f

New:

* Initial release.
